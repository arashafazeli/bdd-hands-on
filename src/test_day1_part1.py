"""Day 1 Part 1 2020 feature tests."""
from .day1_part1 import get_answer_part1, get_answer_part2
from pytest_bdd import (
    given,
    scenario,
    then,
    when,
    parsers
)


@scenario('features/day1_part1.feature', 'find two entries that sum to 2020 and then multiply those two numbers together')
def test_find_two_entries_that_sum_to_2020_and_then_multiply_those_two_numbers_together():
    """find two entries that sum to 2020 and then multiply those two numbers together."""


@scenario('features/day1_part1.feature', 'find three entries that sum to 2020 and then multiply those three numbers together')
def test_find_three_entries_that_sum_to_2020_and_then_multiply_those_three_numbers_together():
    """find three entries that sum to 2020 and then multiply those three numbers together."""


@given(parsers.parse('I get a list of {numbers}'), target_fixture= "input_data")
def i_get_a_list_of_numbers(numbers):
    """I get a list of <numbers>."""
    return (numbers)


@when(parsers.parse('I find the {number1:d} and {number2:d} which add up 2020 and multiply them.'), target_fixture = "result")
def i_find_the_number1_and_number2_which_add_up_2020_and_multiply_them(input_data):
    """I find the <number1> and <number2> which add up 2020 and multiply them.."""
    return get_answer_part1(input_data)


@when(parsers.parse('I find the {number1:d} and {number2:d} and {number3:d} which add up 2020 and multiply them.'), target_fixture = "result")
def i_find_the_number1_and_number2_and_number3_which_add_up_2020_and_multiply_them(input_data):
    """I find the <number1> and <number2> and <number3> which add up 2020 and multiply them.."""
    return get_answer_part2(input_data)


@then(parsers.parse('I will get a {answer:d}'))
def i_will_get_a_answer(result, answer):
    """I will get a <answer>."""
    assert result == answer

"""Day 1 Part 2 2020 feature tests."""
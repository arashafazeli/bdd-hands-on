Feature: Day 1 Part 1 2020
   A report repair. You find the two entries that sum to 2020 and then multiply those two numbers together.

    Scenario Outline: find two entries that sum to 2020 and then multiply those two numbers together
        Given I get a list of <numbers>
        When I find the <number1> and <number2> which add up 2020 and multiply them.
        Then I will get a <answer>
        Examples:
            | answer |  number1  | number2  |            numbers           |
            | 514579 |    299    |   1721   |  1721 979 366 299 675 1456   | 
            | 466564 |    266    |   1754   |  455 677 1275 266 1754 68    |      

        Scenario Outline: find three entries that sum to 2020 and then multiply those three numbers together
        Given I get a list of <numbers>
        When I find the <number1> and <number2> and <number3> which add up 2020 and multiply them.
        Then I will get a <answer>
        Examples:
            |   answer  |  number1  | number2  | number3 |          numbers             |
            | 241861950 |    675    |   366    |   979   |  1721 979 366 299 675 1456   | 
            | 58695900  |    677    |   1275   |   68    |  455 677 1275 266 1754 68    | 
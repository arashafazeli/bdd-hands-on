"""AoC Day 1 Part 1 2020 feat."""

def get_answer_part1(num):
    num = list(num.split(" "))
    num = [int(i.split("\n")[0]) for i in num]
    for i in num:      
        if 2020-i in num:
            number1 = i
            number2 = (2020-i)
            result = int(number1 * number2)
            return(result)


"""AoC Day 1 Part 2 2020 feat."""


def get_answer_part2(num):
    num = list(num.split(" "))
    num = [int(i.split("\n")[0]) for i in num]
    for i in num:      
        for j in num:
            if (2020-i - j) in num:
                number1 = i
                number2 = j
                number3 = (2020-i-j)
                result = int(number1 * number2 * number3)
                return(result)